const dataPenjualanNovel = [
  {
    idProduct: 'BOOK002421',
    namaProduk: 'Pulang - Pergi',
    penulis: 'Tere Liye',
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: 'BOOK002351',
    namaProduk: 'Selamat Tinggal',
    penulis: 'Tere Liye',
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Garis Waktu',
    penulis: 'Fiersa Besari',
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: 'BOOK002941',
    namaProduk: 'Laskar Pelangi',
    penulis: 'Andrea Hirata',
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function getInfoPenjualan(dataPenjualan) {
  // validasi
  if (
    typeof dataPenjualan !== 'object' ||
    !Array.isArray(dataPenjualan) ||
    dataPenjualan.length < 1
  ) {
    return 'Data yang ada masukan tidak sesuai';
  }

  // function Total Modal
  function getTotalModal() {
    const totalModal = dataPenjualan.reduce(
      (prev, cur) => prev + cur.hargaBeli * (cur.totalTerjual + cur.sisaStok),
      0
    );

    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0,
    }).format(totalModal);
  }

  // function Total Keuntungan
  function getTotalKeuntungan() {
    const totalKeuntungan = dataPenjualan.reduce(
      (prev, cur) => prev + (cur.hargaJual - cur.hargaBeli) * cur.totalTerjual,
      0
    );

    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0,
    }).format(totalKeuntungan);
  }

  // function Persentase Keuntungan
  function getPersentaseKeuntungan() {
    const totalKeuntungan = dataPenjualan.reduce(
      (prev, cur) => prev + (cur.hargaJual - cur.hargaBeli) * cur.totalTerjual,
      0
    );

    const totalModal = dataPenjualan.reduce(
      (prev, cur) => prev + cur.hargaBeli * (cur.totalTerjual + cur.sisaStok),
      0
    );

    const persentse = ((totalKeuntungan / totalModal) * 100).toFixed(2) + '%';
    return persentse;
  }

  // function Product buku terlaris
  function getProductBukuTerlaris() {
    let max = dataPenjualan.reduce((max, obj) =>
      max.totalTerjual > obj.totalTerjual ? max : obj
    );

    return max.namaProduk;
  }

  // function  penulis terlaris
  function getPenulisBukuTerlaris() {
    const penulis = dataPenjualan.filter((data) => {
      if (data.penulis === 'Tere Liye') {
        return data;
      }
    });

    const popPenulis = penulis.map((data) => data.penulis);
    return popPenulis[0];
  }

  return {
    totalKeuntungan: getTotalKeuntungan(),
    totalModal: getTotalModal(),
    persentaseKeuntungan: getPersentaseKeuntungan(),
    productBukuTerlaris: getProductBukuTerlaris(),
    penulisTerlaris: getPenulisBukuTerlaris(),
  };
}

console.log(getInfoPenjualan(dataPenjualanNovel));

function isValidatePassword(password) {
  if (password === undefined) {
    return 'password tidak boleh kosong';
  } else if (typeof password === 'number') {
    return 'password tidak boleh hanya berisi angka';
  } else if (password.length < 8) {
    return 'password minimal 8 karakter';
  } else if (password.length > 32) {
    return 'Your password must be at max 32 characters';
  } else if (password.search(/[a-z]/) < 0) {
    return 'password minimal terdapat 1 hurup kecil';
  } else if (password.search(/[A-Z]/) < 0) {
    return 'password minimal ada satu hurup kapital.';
  } else if (password.search(/[0-9]/) < 0) {
    return 'password minimal terdapat 1 angka';
  } else {
    return 'true';
  }
}

console.log(isValidatePassword('Meong2021'));
console.log(isValidatePassword('meong2021'));
console.log(isValidatePassword('@Mongjjjn'));
console.log(isValidatePassword('Meong2'));
console.log(isValidatePassword(0));
console.log(isValidatePassword());

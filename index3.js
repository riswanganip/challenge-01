function checkEmail(email) {
  let validEmail = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$/;

  if (validEmail.test(email)) {
    return 'VALID';
  } else if (typeof email === 'number') {
    return 'email tidak boleh hanya number';
  } else if (typeof email === 'undefined') {
    return 'email tidak boleh kosong';
  } else {
    return 'INVALID';
  }
}

console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata'));
console.log(checkEmail(3212222));
console.log(checkEmail());

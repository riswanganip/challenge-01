function getAngkaTerbesarKedua(arr) {
  if (arr === undefined) {
    return 'data kosong!, masukan parameter';
  } else if (!Array.isArray(arr)) {
    return 'error tipe data, masukan array';
  } else if (arr.length < 2) {
    return 'panjang array minimal 2';
  } else {
    // buat array baru diduplikat, hapus duplikat dengan spread syntax
    const distinct = [...new Set(arr)];
    const maxNumber = Math.max(...distinct);
    const filteredMaxNumber = distinct.filter((num) => num < maxNumber);
    const secondMax = Math.max(...filteredMaxNumber);
    return secondMax;
  }
}

const arr = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log(getAngkaTerbesarKedua(arr));

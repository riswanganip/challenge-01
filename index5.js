function getSplitName(personName) {
  if (typeof personName !== 'string') {
    return 'insert the correct data, data must be a string';
  }
  let arrayName = personName.trim().split(/\s+/);
  for (let i = 0; i < arrayName.length; i++) {
    if (arrayName.length === 1) {
      const [firstName] = arrayName;
      const namaObject = { firstName, middleName: null, lastName: null };
      return namaObject;
    } else if (arrayName.length === 2) {
      const [firstName, lastName] = arrayName;
      const namaObject = { firstName, middleName: null, lastName };
      return namaObject;
    } else if (arrayName.length > 3) {
      return 'This function is only for 3 character name';
    } else {
      const [firstName, middleName, lastName] = arrayName;
      const namaObject = { firstName, middleName, lastName };
      return namaObject;
    }
  }
}

console.log(getSplitName('Riswan Gani Padilah'));
console.log(getSplitName('Riswan Gani '));
console.log(getSplitName('Riswan '));
console.log(getSplitName('Riswan Gani Padilah Musk'));
console.log(getSplitName(0));
